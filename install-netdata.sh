#!/bin/bash
# INSTALACAO NETDATA MONITORAMENTO
yum install git wget vim -y
sleep 1
git clone https://github.com/firehol/netdata.git --depth=1
sleep 2
yum install libuuid-devel -y
sleep 2
cd netdata/
./netdata-installer.sh
